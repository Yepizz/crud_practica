<?php include("db.php"); ?>

<?php include("colores/color.php"); ?>

<?php include('includes/header.php'); ?>

<main class="container p-4">
  <div class="row">
    <div class="col-md-4">
      <!-- MESSAGES -->

      <?php if (isset($_SESSION['message'])) { ?>
      <div class="alert alert-<?= $_SESSION['message_type']?> alert-dismissible fade show" role="alert">
        <?= $_SESSION['message']?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php session_unset(); } ?>

      <!-- ADD TASK FORM -->
      <div class="card card-body">
        <form action="save_task.php" method="POST">
          <div class="form-group">
            <input type="text" name="nombre" class="form-control" placeholder="Nombre del Usuario" autofocus>
          </div>
          <div class="form-group">
            <input type="text" name="apellido_paterno" class="form-control" placeholder="Apellido Paterno" >
          </div>
          <div class="form-group">
            <input type="text" name="apellido_materno" class="form-control" placeholder="Apellido Materno" >
          </div>
          <div class="form-group">
            <input type="text" name="correo" class="form-control" placeholder="Correo Electrónico" >
          </div>
          <div class="form-group">
            <input type="text" name="celular" class="form-control" placeholder="Celular" >
          </div>
          <div class="form-group">
            <input type="text" name="telefono" class="form-control" placeholder="Télefono" >
          </div>
          <div class="form-group">
            <input type="text" name="codigo_postal" class="form-control" placeholder="Código Postal" >
          </div>
          <div class="form-group">
            <input type="text" name="colonia" class="form-control" placeholder="Colonia" >
          </div>
          <div class="form-group">
            <input type="text" name="localidad" class="form-control" placeholder="Localidad" >
          </div>
          <div class="form-group">
            <input type="text" name="municipio" class="form-control" placeholder="Municipio" >
          </div>
          <div class="form-group">
            <input type="text" name="fraccionamiento" class="form-control" placeholder="Fraccionamiento" >
          </div>
          <div class="form-group">
            <input type="text" name="curp" class="form-control" placeholder="CURP" >
          </div>
          
          <input type="submit" name="save_task" class="btn btn-success btn-block" value="Guardar Usuario">
        </form>
      </div>
    </div>
    <div class="col-md-8">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Apellido Paterno</th>
            <th>Apellido Materno</th>
            <th>Correo</th>
            <th>Celular</th>
            <th>Télefono</th>
            <th>Código Postal</th>
            <th>Colonia</th>
            <th>Localidad</th>
            <th>Municipio</th>
            <th>Fraccionamiento</th>
            <th>Curp</th>
           
          </tr>
        </thead>
        <tbody>

          <?php
          $query = "SELECT * FROM users";
          $result_tasks = mysqli_query($conn, $query);    

          while($row = mysqli_fetch_assoc($result_tasks)) { ?>
          <tr>
            <td><?php echo $row['nombre']; ?></td>
            <td><?php echo $row['apellido_paterno']; ?></td>
            <td><?php echo $row['apellido_materno']; ?></td>
            <td><?php echo $row['correo']; ?></td>
            <td><?php echo $row['celular']; ?></td>
             <td><?php echo $row['telefono']; ?></td>
            <td><?php echo $row['codigo_postal']; ?></td>
            <td><?php echo $row['colonia']; ?></td>
            <td><?php echo $row['localidad']; ?></td>
            <td><?php echo $row['municipio']; ?></td>
             <td><?php echo $row['fraccionamiento']; ?></td>
            <td><?php echo $row['curp']; ?></td>
          
            <td>
              <a href="edit.php?id=<?php echo $row['id']?>" class="btn btn-secondary">
                <i class="fas fa-marker"></i>
              </a>
              <a href="delete_task.php?id=<?php echo $row['id']?>" class="btn btn-danger">
                <i class="far fa-trash-alt"></i>
              </a>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</main>

<?php include('includes/footer.php'); ?>
