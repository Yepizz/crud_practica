<?php
include("db.php");
$nombre = '';
$apellidoPaterno= '';
$apellidoMaterno = '';
$correo= '';
$celular = '';
$telefono = '';
$codigoPostal= '';
$colonia = '';
$localidad= '';
$municipio = '';
$fraccionamiento= '';
$curp = '';


if  (isset($_GET['id'])) {
  $id = $_GET['id'];
  $query = "SELECT * FROM users WHERE id=$id";
  $result = mysqli_query($conn, $query);
  if (mysqli_num_rows($result) == 1) {
    $row = mysqli_fetch_array($result);
    $nombre = $row['nombre'];
    $apellidoPaterno = $row['apellido_paterno'];
    $apellidoMaterno = $row['apellido_materno'];
    $correo = $row['correo'];
    $celular = $row['celular'];
    $telefono = $row['telefono'];
    $codigoPostal = $row['codigo_postal'];
    $colonia = $row['colonia'];
    $localidad = $row['localidad'];
    $municipio = $row['municipio'];
    $fraccionamiento = $row['fraccionamiento'];
    $curp = $row['curp'];
  }
}

if (isset($_POST['update'])) {
  $id = $_GET['id'];
  $nombre= $_POST['nombre'];
  $apellidoPaterno = $_POST['apellido_paterno'];
  $apellidoMaterno = $_POST['apellido_materno'];
  $correo= $_POST['correo'];
  $celular= $_POST['celular'];
  $telefono = $_POST['telefono'];
  $codigoPostal= $_POST['codigo_postal'];
  $colonia = $_POST['colonia'];
  $localidad = $_POST['localidad'];
  $municipio = $_POST['municipio'];
  $fraccionamiento= $_POST['fraccionamiento'];
  $curp= $_POST['curp'];
  

  $query = "UPDATE users SET nombre = '$nombre', apellido_paterno = '$apellidoPaterno', apellido_materno = '$apellidoMaterno', correo = '$correo', celular = '$celular', telefono = '$telefono', codigo_postal = '$codigoPostal', colonia = '$colonia', localidad = '$localidad', municipio = '$municipio', fraccionamiento = '$fraccionamiento', curp = '$curp' WHERE id=$id";
  mysqli_query($conn, $query);
  $_SESSION['message'] = 'Usuario Actualizado Correctamente';
  $_SESSION['message_type'] = 'warning';
  header('Location: index.php');
}

?>
<?php include('includes/header.php'); ?>
<div class="container p-4">
  <div class="row">
    <div class="col-md-4 mx-auto">
      <div class="card card-body">
      <form action="edit.php?id=<?php echo $_GET['id']; ?>" method="POST">
        <div class="form-group">
          <input name="nombre" type="text" class="form-control" value="<?php echo $nombre; ?>" placeholder="Actualizar Nombre">
        </div>
         <div class="form-group">
          <input name="apellido_paterno" type="text" class="form-control" value="<?php echo $apellidoPaterno; ?>" placeholder="Actualizar Apellido Paterno">
        </div>
        <div class="form-group">
          <input name="apellido_materno" type="text" class="form-control" value="<?php echo $apellidoMaterno; ?>" placeholder="Actualizar Apellido Materno">
        </div>
        <div class="form-group">
          <input name="correo" type="text" class="form-control" value="<?php echo $correo; ?>" placeholder="Actualizar Correo Electrónico">
        </div>
        <div class="form-group">
          <input name="celular" type="text" class="form-control" value="<?php echo $celular; ?>" placeholder="Actualizar Celular">
        </div>
         <div class="form-group">
          <input name="telefono" type="text" class="form-control" value="<?php echo $telefono; ?>" placeholder="Actualizar Télefono">
        </div>
         <div class="form-group">
          <input name="codigo_postal" type="text" class="form-control" value="<?php echo $codigoPostal; ?>" placeholder="Actualizar Código Postal">
        </div>
         <div class="form-group">
          <input name="colonia" type="text" class="form-control" value="<?php echo $colonia; ?>" placeholder="Actualizar Colonia">
        </div>
         <div class="form-group">
          <input name="localidad" type="text" class="form-control" value="<?php echo $localidad; ?>" placeholder="Actualizar Localidad">
        </div>
         <div class="form-group">
          <input name="municipio" type="text" class="form-control" value="<?php echo $municipio; ?>" placeholder="Actualizar Municipio">
        </div>
         <div class="form-group">
          <input name="fraccionamiento" type="text" class="form-control" value="<?php echo $fraccionamiento; ?>" placeholder="Actualizar Fraccionamiento">
        </div>
         <div class="form-group">
          <input name="curp" type="text" class="form-control" value="<?php echo $curp; ?>" placeholder="Actualizar Curp">
        </div>
        <button class="btn-success" name="update">
          Actualizar Usuario
</button>
      </form>
      </div>
    </div>
  </div>
</div>
<?php include('includes/footer.php'); ?>
